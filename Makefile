# sense8 - buildroot

BUILDROOT_DIR=buildroot-2020.02.9
CONFIG_DIR=$(shell pwd)/config
DEFCONFIG_PATH=$(CONFIG_DIR)/sense8-defconfig
EXTERNAL_TREE=sense8-tree
EXTERNAL_TREE_PATH=$(shell pwd)/$(EXTERNAL_TREE)

.PHONY: help
help:
	$(info "CONFIG_DIR: $(CONFIG_DIR)")

.PHONY: savedefconfig
savedefconfig:
	cd $(BUILDROOT_DIR); \
		[ -e $(DEFCONFIG_PATH) ] && cp $(DEFCONFIG_PATH) $(DEFCONFIG_PATH).bak; \
		make savedefconfig BR2_DEFCONFIG=$(CONFIG_DIR)/sense8-defconfig

.PHONY: build
build:
		cd $(BUILDROOT_DIR); make

.PHONY: config
config:
		cd $(BUILDROOT_DIR); make menuconfig

.PHONY: linux-config
linux-config:
		cd $(BUILDROOT_DIR); make linux-menuconfig

.PHONY: set_external_tree
set_external_tree:
	cd $(BUILDROOT_DIR); \
		make BR2_EXTERNAL=$(EXTERNAL_TREE_PATH) menuconfig

.PHONY: unset_external_tree
unset_external_tree:
	cd $(BUILDROOT_DIR); \
		make BR2_EXTERNAL= menuconfig
