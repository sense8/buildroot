# sense8 - buildroot

Build image for raspberry pi zero w

## wifi

`./configure` will setup wpa_supplicant.conf with SSID and passphrase.


## Links

* [exploring cgroups](https://downey.io/blog/exploring-cgroups-raspberry-pi/)
* [pi enable cgroups](https://gist.github.com/bkmeneguello/c29da541d3183c26c792d628a46ad826)
* [An introduction to cgroups and cgroupspy ](https://events.static.linuxfound.org/sites/events/files/slides/cgroups_0.pdf)
* [Gentoo - RPI](https://wiki.gentoo.org/wiki/Raspberry_Pi)
* [Raspberrypi UART configuration](https://www.raspberrypi.org/documentation/configuration/uart.md)