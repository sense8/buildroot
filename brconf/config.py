config = {
    "machine": "raspberrypi0w",
}


def gv(conf, key):
    root = conf
    for subkey in key.split("."):
        root = root[subkey]
    return root


def merge_config(*confs):
    nc = {}
    if len(confs) == 0:
        return nc
    [nc.update(c) for c in [config, *confs]]
    return nc