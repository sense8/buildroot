import os
from brconf import br_external_tree_dir
from brconf.utils import read_file, write_file, ask, confirm
from brconf.config import gv

config = {
    "sense8": {
        "etc": {
            "influxdb": "board/{machine}/rootfs_overlay/sense8/supervisor/etc/influxdb.env"
        }
    }
}


def make_influxdb_secret_path(config):
    machine = gv(config, "machine")
    return os.path.join(
        br_external_tree_dir,
        gv(config, "sense8.etc.influxdb").format(machine=machine),
    )


def setup_influxdb_secrets(config):
    secret_path = make_influxdb_secret_path(config)
    print("secret_path: {}".format(secret_path))
    if os.path.exists(secret_path):
        print("[i] Current configuration")
        print("[i] {}\n".format(secret_path))
        print(read_file(secret_path))
    wpa_path_sample = "{}.sample".format(secret_path)
    sample = read_file(wpa_path_sample)
    host = ask("host")
    token = ask("token")
    org = ask("email")
    bucket = ask("bucket")
    write_file(
        secret_path, sample.format(HOST=host, TOKEN=token, ORG=org, BUCKET=bucket)
    )
    print(read_file(secret_path))


def setup(config):
    setup_influxdb_secrets(config)
