import re


def read_file(file_path):
    with open(file_path, "r") as rh:
        return rh.read()


def write_file(file_path, data):
    with open(file_path, "w") as wh:
        wh.write(data)


def confirm(text="ok?"):
    answer = ""
    pattern = re.compile("(y(es)?|n(o)?)$")
    while not pattern.match(answer):
        answer = input("{} (y/n)? ".format(text))
    if answer.lower().startswith("n"):
        return False
    return True


def ask(question):
    first_pass = True
    response = None
    while first_pass or not confirm():
        first_pass = False
        response = input("{}? ".format(question))
    return response.strip()