import os

from brconf import base_path, br_external_tree_dir
from brconf.utils import read_file, write_file, confirm, ask
from brconf.config import gv

config = {
    "wifi": {
        "wpa_supplicant_conf": "board/{machine}/rootfs_overlay/etc/wpa_supplicant.conf"
    }
}


def make_wpa_supplicant_path(config):
    file_path = gv(config, "wifi.wpa_supplicant_conf")
    machine = gv(config, "machine")
    return os.path.join(br_external_tree_dir, file_path.format(machine=machine))


def setup(config):
    wpa_path = make_wpa_supplicant_path(config)
    if os.path.exists(wpa_path):
        print("[i] Current configuration")
        print("[i] {}\n".format(wpa_path))
        print(read_file(wpa_path))
    wpa_path_sample = "{}.sample".format(make_wpa_supplicant_path(config))
    sample = read_file(wpa_path_sample)
    ssid = ask("SSID")
    passphrase = ask("passphrase")
    write_file(wpa_path, sample.format(SSID=ssid, PASSPHRASE=passphrase))
    print(read_file(wpa_path))
