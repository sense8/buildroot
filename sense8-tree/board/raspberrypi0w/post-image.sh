#!/bin/bash

DOCKER_PLATFORM=linux/arm/v6
DOCKER_PLATFORM_OPT="--platform $DOCKER_PLATFORM"
DOCKER_IMAGES="showi/sense8-supervisor:0.0.1 showi/sense8-redis:0.0.1 showi/sense8-telegraf:0.0.1 showi/sense8-collector:0.0.1"

FWDIR="${BINARIES_DIR}/rpi-firmware"
CMDLINE_TXT="${FWDIR}/cmdline.txt"
CONFIG_TXT="${FWDIR}/config.txt"
CONFIG_TXT_HEADER="# Sense8 custom"
IMAGES_DIR="${BASE_DIR}/target/sense8/images"
CONTAINERS_DIR="${BASE_DIR}/target/sense8/containers"

mkdir -p $FWDIR
mkdir -p $IMAGES_DIR

rm -f $IMAGES_DIR/*.tar.gz

# for docker_image in $DOCKER_IMAGES; do
# 	docker pull --platform $DOCKER_PLATFORM $docker_image
# 	save_path="${IMAGES_DIR}/$(echo $docker_image | tr '/' '-' | tr ':' '-').tar.gz"
# 	echo "[i] ($DOCKER_PLATFORM) dump docker image ${docker_image} to $save_path"
# 	docker save $docker_image | gzip >  $save_path
# 	[ $? -ne 0 ] && echo "[-] docker export error" && exit 1
# done

# echo "[i] cloning containers in ${CONTAINERS_DIR}"
# git clone https://gitlab.com/sense8/containers.git $CONTAINERS_DIR

# config.txt
grep -Fqx "$CONFIG_TXT_HEADER" "$CONFIG_TXT"
if [ $? -ne 0 ]
then
	cat << __EOF__ >> $CONFIG_TXT

${CONFIG_TXT_HEADER}

gpu_mem_256=64
gpu_mem_512=64
gpu_mem_1024=64

# Try auto load bt firmware
dtparam=krnbt=on

# Enable I2C
dtparam=i2c1=on

# Safe HDMI
hdmi_safe=1


# Fail to boot
# core_freq=500
# core_freq_min=500

# fixes rpi (3B, 3B+, 3A+, 4B and Zero W) ttyAMA0 serial console
# dtoverlay=miniuart-bt

__EOF__
fi

# cmdline.txt
# hciattach /dev/ttyAMA0 bcm43xx 921600 flow - 
BASE_OPTS="root=/dev/mmcblk0p2 console=tty1 console=115200"
# BASE_OPTS="root=/dev/mmcblk0p2 console=serial0,115200 console=tty1 elevator=deadline fsck.repair=1"
# root=/dev/mmcblk0p2 console=tty1 console=serial0,115200 cgroup_enable=memory,cpu,devices,cpuacct,freezer,blkio swapaccount=1 rootwait
CGROUPS_OPTS="cgroup_enable=memory,cpu,devices,cpuacct,freezer,blkio swapaccount=1"
echo -n "$BASE_OPTS $CGROUPS_OPTS rootwait" > $CMDLINE_TXT

# debug output
echo "[i] cmdline.txt"
cat $CMDLINE_TXT
echo
echo "[i] config.txt"
cat $CONFIG_TXT

exit $?
