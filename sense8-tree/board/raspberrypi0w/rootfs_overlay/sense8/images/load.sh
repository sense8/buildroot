#!/bin/sh

function wait_docker {
    while true
    do
        echo "Wait for docker daemon"
        docker info > /dev/null
        [ $? -eq 0 ] && break
        sleep 2
    done
}

wait_docker

DOCKER_IMAGES="/sense8/images/showi-sense8-*.tar.gz"
for DOCKER_IMAGE in $(ls $DOCKER_IMAGES);
do
    echo "[i] loading docker image $DOCKER_IMAGE"
    gunzip -c $DOCKER_IMAGE | docker load
done