#!/bin/sh

docker run \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v /sense8/containers/:/sense8/containers \
      -v /sense8/supervisor/etc:/sense8/supervisor/etc \
      --device=/dev/i2c-1 \
      -it ${IMAGE} ./sense8/supervisor/run-ansible.sh


docker run -v /var/run/docker.sock:/var/run/docker.sock -v /sense8/supervisor/etc:/sense8/supervisor/etc --device=/dev/i2c-1 -it ${IMAGE} /sense8/supervisor/run-ansible.sh
docker run -v /var/run/docker.sock:/var/run/docker.sock -v /sense8/supervisor/etc:/sense8/supervisor/etc --device=/dev/i2c-1 -it ${IMAGE} /bin/sh