################################################################################
#
# python-ansible
#
################################################################################

PYTHON_ANSIBLE_VERSION = 436d8a13b48de1115a05a056aac6daa3e979e7f5
PYTHON_ANSIBLE_SITE = https://github.com/ansible/ansible.git
PYTHON_ANSIBLE_SITE_METHOD=git
PYTHON_ANSIBLE_LICENSE = GNU-GPL-V3
PYTHON_ANSIBLE_LICENSE_FILES = ansible/COPYING
PYTHON_ANSIBLE_DEPENDENCIES = python-cryptography python-pyyaml python-jinja2
PYTHON_ANSIBLE_SETUP_TYPE = distutils

$(eval $(python-package))
