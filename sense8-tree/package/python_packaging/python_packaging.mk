################################################################################
#
# python-packaging
#
################################################################################

PYTHON_PACKAGING_VERSION = d93c6eea7d2caa637176426d255895bdf7db4f64
PYTHON_PACKAGING_SITE = https://github.com/pypa/packaging.git
PYTHON_PACKAGING_SITE_METHOD= git
PYTHON_PACKAGING_LICENSE = BSD
PYTHON_PACKAGING_LICENSE_FILES = LICENSE
PYTHON_PACKAGING_DEPENDENCIES =
PYTHON_PACKAGING_SETUP_TYPE = distutils

$(eval $(python-package))
