SENSE8_VERSION=b1d2b5a77f4841f40afb816c8ff53bbc313db5f9
SENSE8_SITE=https://gitlab.com/sense8/supervisors.git
SENSE8_SITE_METHOD=git
SENSE8_INSTALL_TARGET=YES

define SENSE8_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/sense8/supervisor/etc
	cp -r $(SENSE8_DIR)/* $(TARGET_DIR)/sense8/supervisor
endef

$(eval $(generic-package))
